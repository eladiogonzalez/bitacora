var express = require('express'),
app = express(),
port = process.env.PORT||3000;

var path = require('path');

app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log("bitacora en puerto: " + port);

//desde aqui le indicamos cuando llegue a raiz realice el get

app.get('/', function (req, res) {
//  res.sendFile(path.join(__dirname, 'index.html'));
// para hacer nuestra aplicacion polymer modo node, invocamos al indez.html
// de polymer, inicialmente ejecutanmos el npm init para crear el modo node
// posteriormente hicimos el cambio en el .js para incorporar el. html
  res.sendFile('index.html', {root: '.'});

});
